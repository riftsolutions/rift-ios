
//
//  RearMenuViewController.swift
//  RIFT
//
//  Created by McDowell, Ian J [ITACD] on 3/2/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit
import RESideMenu

class RearMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView = {
            let tv = UITableView()
            
            tv.backgroundColor = UIColor.clearColor()
            tv.separatorStyle = .None
            
            tv.dataSource = self
            tv.delegate = self
            
            tv.translatesAutoresizingMaskIntoConstraints = false
            
            self.view.addSubview(tv)
            
            tv.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor).active = true
            tv.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor).active = true
            tv.topAnchor.constraintEqualToAnchor(self.view.topAnchor).active = true
            tv.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor).active = true
            
            return tv
        }()
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(NSStringFromClass(UITableViewCell.self))
        
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: NSStringFromClass(UITableViewCell.self))
            cell?.backgroundColor = UIColor.clearColor()
            cell?.textLabel?.textColor = UIColor.whiteColor()
            cell?.selectedBackgroundView = UIView()
            cell?.selectedBackgroundView?.backgroundColor = UIColor(white: 0.0, alpha: 0.2)
        }
        
        switch indexPath.row {
        case 0:
            cell?.textLabel?.text = "Top Stories"
        case 1:
            cell?.textLabel?.text = "Digest"
        case 2:
            cell?.textLabel?.text = "My Saves"
        case 3:
            cell?.textLabel?.text = "Notifications"
        default:
            break
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var contentHeight: CGFloat = 0
        
        for section in 0..<self.numberOfSectionsInTableView(tableView) {
            for row in 0..<self.tableView(tableView, numberOfRowsInSection: section) {
                let indexPath = NSIndexPath(forRow: row, inSection: section)
                contentHeight += self.tableView(tableView, heightForRowAtIndexPath: indexPath)
            }
        }
        
        return (tableView.bounds.size.height - contentHeight) / 2
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let sideMenu = appDelegate.window?.rootViewController as! RESideMenu
        
        sideMenu.hideMenuViewController()
    }
    
}
