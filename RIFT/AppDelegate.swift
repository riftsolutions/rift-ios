//
//  AppDelegate.swift
//  RIFT
//
//  Created by McDowell, Ian J [ITACD] on 3/1/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit
import RESideMenu
import BWWalkthrough

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, BWWalkthroughViewControllerDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        let userDefaults = NSUserDefaults()
        let hasShownWalkthrough = userDefaults.boolForKey("has_shown_walkthrough")
        if !hasShownWalkthrough {
            self.showWalkthrough()
            return true
        }
        
        self.showMainContent()

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    
    func showWalkthrough() {
        let storyboard = UIStoryboard(name: "Walkthrough", bundle: nil)
        let walkthrough = storyboard.instantiateInitialViewController() as! BWWalkthroughViewController
        let page1 = storyboard.instantiateViewControllerWithIdentifier("page1")
        let page2 = storyboard.instantiateViewControllerWithIdentifier("page2")
        
        walkthrough.delegate = self
        
        walkthrough.addViewController(page1)
        walkthrough.addViewController(page2)
        
        self.window?.rootViewController = walkthrough
    }
    
    func showMainContent() {
        let contentViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        let leftMenuViewController = RearMenuViewController()
        
        let sideMenuViewController = RESideMenu(contentViewController: contentViewController, leftMenuViewController: leftMenuViewController, rightMenuViewController: nil)
        
        sideMenuViewController.backgroundImage = UIImage(named: "bg")
        sideMenuViewController.menuPreferredStatusBarStyle = .LightContent
        
        self.window?.rootViewController = sideMenuViewController
    }
    
    
    
    func walkthroughCloseButtonPressed() {
        self.showMainContent()
    }
    
}

class WalkthroughViewController: BWWalkthroughViewController {
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
}